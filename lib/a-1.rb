# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  result_array = []

  nums.each_with_index do |num, idx|
    # return (num + 1...nums[idx+1])
    if num + 1 != nums[idx+1] && idx != nums.length - 1
      (num + 1...nums[idx+1]).each {|number| result_array << number}

    end
  end
  result_array
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  binary = binary.split("").reverse

  binary.map!.with_index {|el, idx| (el.to_i) * 2 ** idx}
  binary.reduce(:+)

end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hash = Hash.new
    self.each do |k , v|
      if prc.call(k, v)
        hash[k] = v
      end
    end
    hash
  end

end



  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  class Hash

    def my_merge(hash, &prc)

      result_hash = {}

      if block_given?
        hash.each do |key, v|
          oldval = hash[key]
          if self.key?(key)
            newval = self[key]
          else
            newval = 0
          end
          result_hash[key] = prc.call(key, newval, oldval)
        end
        return result_hash
      else
        hash.each do |k, v|
          self[k] = v
        end
        return self
      end
    end
  end


# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  #2 = 0.. there has to be a formula.. can't create an array like this
  # let's say I enter "3" then my answer is 4 because 1 + 3 = 4... now I could say start with
  # 2 then add 1 then add both to get 3.. okay lets try that

  array = [2, 1]

  return array[n] if n == 0 || n == 1

  while array.length - 1 != n.abs
    array << array[-1] + array[-2]
  end
  array[-1]

end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  string = string.split("")

  result = ""

  string.each_with_index do |letter, idx|
    string.each_with_index do |secondary_letter, idx_2|
      if secondary_letter == letter && idx_2 > idx
        word = string[idx..idx_2]
        if word == word.reverse && word.length > result.length
          result = word
        end
      end
    end
 end
 return false if result.length < 3
  result.length
end
